﻿using GooglePlayGames;
using GooglePlayGames.BasicApi;
using TMPro;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static PlayGamesPlatform platform;
    public TextMeshProUGUI text;
    void Start()
    {
        if (platform == null)
        {
            PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder().Build();
            PlayGamesPlatform.InitializeInstance(config);
            PlayGamesPlatform.DebugLogEnabled = true;
            platform = PlayGamesPlatform.Activate();
            text.text = "Loading ...";
        }

        Social.Active.localUser.Authenticate(success =>
        {
            text.text = $"Loaded {success}, user: {Social.localUser.ToString()}.";
            if (success)
            {
                Debug.Log($"Logged in successfully, username: {Social.localUser.userName}");
            }
            else
            {
                Debug.Log("Login Failed");
            }
        });
    }

    // Update is called once per frame
    void Update()
    {

    }
}
